var count = 1;
var box = 1;
var interval = 15;
var yellow = interval - 5;
function lights(){
    $('#counter').html(count);
    if(box==1){
        $('#b4-l3').removeClass('bg-success').addClass('bg-dark');
        $('#b4-l1').removeClass('bg-dark').addClass('bg-danger');
        $('#b1-l1').removeClass('bg-danger').addClass('bg-dark');
        $('#b1-l3').removeClass('bg-dark').addClass('bg-success');
        if(count==yellow){
            $('#b1-l2').removeClass('bg-dark').addClass('bg-warning');
            $('#b2-l2').removeClass('bg-dark').addClass('bg-warning');
        }
        if(count==interval){
            $('#b1-l2').removeClass('bg-warning').addClass('bg-dark');
            $('#b2-l2').removeClass('bg-warning').addClass('bg-dark');
            count = 0;
            box = 2;
        }
    }else if(box==2){
        $('#b1-l3').removeClass('bg-success').addClass('bg-dark');
        $('#b1-l1').removeClass('bg-dark').addClass('bg-danger');
        $('#b2-l1').removeClass('bg-danger').addClass('bg-dark');
        $('#b2-l3').removeClass('bg-dark').addClass('bg-success');
        if(count==yellow){
            $('#b2-l2').removeClass('bg-dark').addClass('bg-warning');
            $('#b3-l2').removeClass('bg-dark').addClass('bg-warning');
        }
        if(count==interval){
            $('#b2-l2').removeClass('bg-warning').addClass('bg-dark');
            $('#b3-l2').removeClass('bg-warning').addClass('bg-dark');
            count = 0;
            box = 3;
        }
    }else if(box==3){
        $('#b2-l3').removeClass('bg-success').addClass('bg-dark');
        $('#b2-l1').removeClass('bg-dark').addClass('bg-danger');
        $('#b3-l1').removeClass('bg-danger').addClass('bg-dark');
        $('#b3-l3').removeClass('bg-dark').addClass('bg-success');
        if(count==yellow){
            $('#b3-l2').removeClass('bg-dark').addClass('bg-warning');
            $('#b4-l2').removeClass('bg-dark').addClass('bg-warning');
        }
        if(count==interval){
            $('#b3-l2').removeClass('bg-warning').addClass('bg-dark');
            $('#b4-l2').removeClass('bg-warning').addClass('bg-dark');
            count = 0;
            box = 4;
        }
    }else if(box==4){
        $('#b3-l3').removeClass('bg-success').addClass('bg-dark');
        $('#b3-l1').removeClass('bg-dark').addClass('bg-danger');
        $('#b4-l1').removeClass('bg-danger').addClass('bg-dark');
        $('#b4-l3').removeClass('bg-dark').addClass('bg-success');
        if(count==yellow){
            $('#b4-l2').removeClass('bg-dark').addClass('bg-warning');
            $('#b1-l2').removeClass('bg-dark').addClass('bg-warning');
        }
        if(count==interval){
            $('#b4-l2').removeClass('bg-warning').addClass('bg-dark');
            $('#b1-l2').removeClass('bg-warning').addClass('bg-dark');
            count = 0;
            box = 1;
        }
    }
    count++;
}
setInterval(lights, 100);